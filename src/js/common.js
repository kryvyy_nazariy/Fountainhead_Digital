// fixed svg show
//-----------------------------------------------------------------------------
svg4everybody();

// placeholder
//-----------------------------------------------------------------------------------
$(function() {
	$('input[placeholder], textarea[placeholder]').placeholder();
});


//slick slider
//--------------------------------------------------------------------------------
function slickSlideInit(){
	var leftArrow = '<div class=" el-left-arrow"><div class="arrow-left icon"></div></div>';
	var rightArrow ='<div class=" el-right-arrow"><div class="arrow-right icon"></div></div>';
	$('.js-portfolio-slider').slick({
		dots: false,
		infinite: true,
		speed: 300,
		slidesToShow: 4,
		slidesToScroll: 1,
		prevArrow: leftArrow,
		nextArrow: rightArrow,
		responsive: [
		  {
			breakpoint: 1280,
			settings: {
				slidesToShow: 3
			}
		  },
		  {
			breakpoint: 992,
			settings: {
				slidesToShow: 2
			}
		  },
		  {
			breakpoint: 767,
			settings: {
				slidesToShow: 1
			}
		  }
		  // You can unslick at a given breakpoint now by adding:
		  // settings: "unslick"
		  // instead of a settings object
		]
	  });
};

function setWidthTextSection(){
	var textSection =document.querySelectorAll('.js-section-text');
	textSection.forEach(function(section){
		section.style.width = section.dataset.width;
	})
}

var sameHeight = function(elem){
	var containerArray =  Array.from(document.querySelectorAll(elem));
	var height = 0;
	containerArray.reduce(function(current, next){
		current.offsetHeight > next.offsetHeight ? 
			height < current.offsetHeight ?  height =  current.offsetHeight : height 
			: 
			height < next.offsetHeight ?  height =  next.offsetHeight : height;
			return next;
	})

	containerArray.forEach(function(element) {
		 element.style.height = height+'px';
	 });
}

//info popup open

var infoPopupOpen = function(){
	var hoverflag= true;
	var timeoutFunction;

	$(document).on('mouseenter','.el-regular-list li', function(e){		
			if(window.innerWidth>1023){
				$(this).find('.el-popup').hasClass('is_visible') ? $('.el-popup').removeClass('is_visible') : false;
					clearTimeout(timeoutFunction);
					// e.stopPropagation();
					$(this).find('.el-popup').addClass('is_visible');
					hoverflag = false;
			}
	});
	$(document).on('mouseleave','.el-regular-list li', function(){
			if(hoverflag){
					$('.el-popup').removeClass('is_visible');
			}else{
					timeoutFunction = setTimeout(function(){
							var curretflag = hoverflag;
							hoverflag = true;
							$('.el-popup').removeClass('is_visible');
					},1000);
			}
	});
	$(document).on('mouseenter','.el-popup', function(){
			hoverflag = true;
			clearTimeout(timeoutFunction);
	});
	$(document).on('mouseleave','.el-popup', function(){
			hoverflag = true;
			$('.el-popup').removeClass('is_visible');
	});

};
//info popup open

//scrol to set section
	var goTo = function(element){
		var currentElemArrey = document.querySelectorAll(element);
		currentElemArrey.forEach(function(item) {
			item.addEventListener('click', function(e){
				e.preventDefault();
				var targetitem = document.querySelector('.'+this.dataset.target);
				var targetPosition = targetitem.offsetTop;
				$('body,html').stop().animate({scrollTop:targetPosition},800);
			})
		}, this);
	}
//

//mobile menu toggle
var mobileMenuToggle = function(){
	$('.js-slide-down-menu').slideUp();
	$('.js-menu-button').on('click', function(e){
		e.stopPropagation();
		$('.js-slide-down-menu').slideToggle();
	})
	document.addEventListener('scroll',function(){
		$('.js-slide-down-menu').slideUp();
	})
	document.addEventListener('click',function(){
		$('.js-slide-down-menu').slideUp();
	})
	$('.js-slide-down-menu').on('click',function(e){
		e.stopPropagation();
	})
} 

var adaptationFlag = null;
if(window.innerWidth < 1024){
	adaptationFlag = true;
}else{
	adaptationFlag = false;
}

var adaptationScripts = function(){
	if(window.innerWidth < 1024 && adaptationFlag){
		adaptationFlag = false;
		if(document.querySelector('.el-regular-section')){
			var sectionEllementArray = Array.from(document.querySelectorAll('.el-regular-section'));
			sectionEllementArray.forEach(function(element) {
				if(element.querySelector('.el-section-img-column') && element.querySelector('.el-section-content-column')){
					var image_column = element.querySelector('.el-section-img-column');
					var content_column = element.querySelector('.el-section-content-column');
					var container = element.querySelector('.container');
					container.insertBefore(image_column, content_column); 
				}				
			}, this);
		}		
	}else if(window.innerWidth > 1024 && !adaptationFlag){
		adaptationFlag = true;
		if(document.querySelector('.el-regular-section')){
			var sectionEllementArray = Array.from(document.querySelectorAll('.el-regular-section'));
			sectionEllementArray.forEach(function(element) {
				if(element.querySelector('.el-section-img-column') && element.querySelector('.el-section-content-column') && element.className.match(/md-right/gi)){
					var image_column = element.querySelector('.el-section-img-column');
					var content_column = element.querySelector('.el-section-content-column');
					var container = element.querySelector('.container');
					container.insertBefore(content_column, image_column ); 
				}				
			}, this);
		}		
	}
}

var openItemList = function(triger, content){	
	if($(content).length && window.innerWidth< 1023){	
		$(content).slideUp();
		$(triger).on('click', function(){
			// $(content).slideUp();
			if($(this).find(content).length && window.innerWidth< 1023){
				$(this).find(content).slideToggle();
				$(this).closest('li').toggleClass('is_open');
			} else if(!$(this).find(content).length && window.innerWidth< 1023){
				$(this).next(content).slideToggle();
			}			
		})
	}
}

$(document).ready(function(){
	goTo('.js-scroll-button');
	slickSlideInit();
	setWidthTextSection();
	sameHeight('.js-same-height');
	infoPopupOpen();
	mobileMenuToggle();
	adaptationScripts();
	openItemList('.js-regular-list li','.js-sliding-content');
	openItemList('.el-footer-column h3','.el-footer-column ul');
});

$(window).resize(function(){
	adaptationScripts();
	if(window.innerWidth > 1023){
		$('.el-footer-column ul').slideDown();
	} else{
		$('.el-footer-column ul').slideUp();
	}
});
